""" Tuple utils. """


class Tuple:

    @staticmethod
    def sum(tuple1, tuple2):
        return tuple(map(lambda x, y: x + y, tuple1, tuple2))

    @staticmethod
    def multiply(tuple1, tuple2):
        return tuple(map(lambda x, y: x * y, tuple1, tuple2))
