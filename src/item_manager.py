import random

from src.item import Item


class ItemManager:

    def __init__(self, total_items, texture):

        self.items = []
        self.total_items = total_items

        self.texture = texture

    ''' Generates the items that are missing. '''

    def generateMissingItems(self, random_position):
        missing_number = self.total_items - len(self.items)

        for i in range(missing_number):
            self.items.append(Item(random_position(), self.texture, random.randrange(1, 4)))

    ''' Says if an item is found at the position. '''

    def has_item_at(self, position):
        return self.find_item_at(position) is not None

    ''' Finds an item by its position. '''

    def find_item_at(self, position):

        for item in self.items:
            if item.position == position:
                return item

        return None

    ''' Remove one item from the list. '''
    def remove(self, item):
        self.items.remove(item)

    ''' Draws the items. '''
    def draw(self, screen):
        for item in self.items:
            item.draw(screen)
