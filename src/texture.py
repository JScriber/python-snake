import pygame

from src.utils import Tuple


class Texture:
    # Size of the grid.
    GRID_SIZE = 64

    # Position of the corners.
    CORNER_1 = (0, 0)
    CORNER_2 = (2, 0)
    CORNER_3 = (2, 2)
    CORNER_4 = (0, 1)

    # Position of the flat parts.
    HORIZONTAL = (1, 0)
    VERTICAL = (2, 1)

    # Position of the heads.
    HEAD_UP = (3, 0)
    HEAD_RIGHT = (4, 0)
    HEAD_DOWN = (4, 1)
    HEAD_LEFT = (3, 1)

    # Position of the tails.
    TAIL_UP = (3, 2)
    TAIL_RIGHT = (4, 2)
    TAIL_DOWN = (4, 3)
    TAIL_LEFT = (3, 3)

    # Items.
    APPLE = (0, 3)

    def __init__(self, paddings):
        self.pack = pygame.image.load("assets/images/snake.png")
        self.paddings = paddings

    ''' Display a part of the asset, on given position. '''
    def display(self, screen, position, asset):

        size = (self.GRID_SIZE,) * 2
        real_position = Tuple.multiply(position, size)
        visible_area = pygame.Rect((real_position[0] + self.paddings[0], real_position[1] + self.paddings[1]), size)

        screen.blit(self.pack, visible_area, (
            # X and Y position from which to crop.
            asset[0] * Texture.GRID_SIZE, asset[1] * Texture.GRID_SIZE,
            # Portion to crop.
            Texture.GRID_SIZE, Texture.GRID_SIZE
        ))
