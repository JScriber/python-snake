import pygame
from enum import Enum

from src.texture import Texture
from src.vector import Vector


class SnakePieceKind(Enum):
    HEAD = 1
    BODY = 2
    TAIL = 3


''' Part of a snake. '''


class SnakePiece:

    def __init__(self, position, vector, texture):

        self.position = position

        self.vector = vector

        self.texture = texture

    ''' Draws the body part on the screen. '''

    def draw(self, screen, kind, next_piece):

        # Determine correct asset to use.
        asset = self._findAsset(kind, next_piece)

        if asset is not None:
            self.texture.display(screen, self.position, asset)

    ''' Determines the correct asset to use given a context. '''

    def _findAsset(self, kind, next_piece):

        asset = None

        if kind == SnakePieceKind.BODY:

            if self.vector == next_piece.vector:
                if self.vector[1] != 0:
                    asset = Texture.VERTICAL
                else:
                    asset = Texture.HORIZONTAL

            elif self.vector == Vector.LEFT and next_piece.vector == Vector.DOWN:
                asset = Texture.CORNER_1

            elif self.vector == Vector.LEFT and next_piece.vector == Vector.UP:
                asset = Texture.CORNER_4

            elif self.vector == Vector.RIGHT and next_piece.vector == Vector.DOWN:
                asset = Texture.CORNER_2

            elif self.vector == Vector.RIGHT and next_piece.vector == Vector.UP:
                asset = Texture.CORNER_3

            elif self.vector == Vector.UP and next_piece.vector == Vector.LEFT:
                asset = Texture.CORNER_2

            elif self.vector == Vector.UP and next_piece.vector == Vector.RIGHT:
                asset = Texture.CORNER_1

            elif self.vector == Vector.DOWN and next_piece.vector == Vector.LEFT:
                asset = Texture.CORNER_3

            elif self.vector == Vector.DOWN and next_piece.vector == Vector.RIGHT:
                asset = Texture.CORNER_4

        elif kind == SnakePieceKind.HEAD:
            if self.vector == Vector.LEFT:
                asset = Texture.HEAD_LEFT

            elif self.vector == Vector.RIGHT:
                asset = Texture.HEAD_RIGHT

            elif self.vector == Vector.UP:
                asset = Texture.HEAD_UP

            elif self.vector == Vector.DOWN:
                asset = Texture.HEAD_DOWN

        elif kind == SnakePieceKind.TAIL:
            if next_piece.vector == Vector.LEFT:
                asset = Texture.TAIL_LEFT

            elif next_piece.vector == Vector.RIGHT:
                asset = Texture.TAIL_RIGHT

            elif next_piece.vector == Vector.UP:
                asset = Texture.TAIL_UP

            elif next_piece.vector == Vector.DOWN:
                asset = Texture.TAIL_DOWN

        return asset
