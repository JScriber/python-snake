from src.utils import Tuple


class Position:

    def __init__(self, x, y):

        self.position = [x, y]

    ''' Updates the position by applying a vector to it. '''

    def apply_vector(self, vector, boundaries):

        self.position = list(Tuple.sum(self.position, vector))

        # Mustn't leave screen on the right.
        if self.position[0] > boundaries[0] - 1:
            self.position[0] = 0

        # Mustn't leave screen on the left.
        if self.position[0] < 0:
            self.position[0] = boundaries[0] - 1

        # Mustn't leave screen on the bottom.
        if self.position[1] > boundaries[1] - 1:
            self.position[1] = 0

        # Mustn't leave screen on the top.
        if self.position[1] < 0:
            self.position[1] = boundaries[1] - 1
