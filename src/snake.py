from src.snake_piece import SnakePiece, SnakePieceKind
from src.vector import Vector


class Snake:

    def __init__(self, initial_position, initial_direction, initial_size, texture):

        self.position = initial_position

        self.__direction = initial_direction

        self.size = initial_size

        self.texture = texture

        self.pieces = []

        self.piecesIndex = 0

    @property
    def direction(self):
        return self.__direction

    @direction.setter
    def direction(self, direction):

        if direction == Vector.UP:
            valid_direction = self.__direction != Vector.DOWN

        elif direction == Vector.DOWN:
            valid_direction = self.__direction != Vector.UP

        elif direction == Vector.LEFT:
            valid_direction = self.__direction != Vector.RIGHT

        elif direction == Vector.RIGHT:
            valid_direction = self.__direction != Vector.LEFT

        else:
            raise Exception('Invalid direction')

        if valid_direction:
            self.__direction = direction

    ''' Makes the snake move. '''

    def move(self, boundaries):

        # Calculate the next position.
        self.position.apply_vector(self.direction, boundaries)

        # Persist current state.
        part = SnakePiece(self.position.position, self.direction, self.texture)

        # Find appropriate action (replace or insert).
        if len(self.pieces) < self.size:
            self.pieces.insert(self.piecesIndex, part)
        else:
            self.pieces[self.piecesIndex] = part

        # Iterate over array.
        self.piecesIndex += 1

        if self.piecesIndex >= self.size:
            self.piecesIndex = 0

    ''' Draws the snake on screen. '''

    def draw(self, screen):

        # Get the previous index as the current refers to the next overwritten place.
        edge_index = self._findPreviousIndex(self.piecesIndex)

        for index, piece in enumerate(self.pieces):

            # Determine the kind.
            if index == edge_index:
                kind = SnakePieceKind.HEAD

            elif index == self.piecesIndex:
                kind = SnakePieceKind.TAIL

            else:
                kind = SnakePieceKind.BODY

            next_piece = None

            # If we are not at the head, find the next piece.
            if index != edge_index:
                next_piece = self.pieces[self._findNextIndex(index)]

            piece.draw(screen, kind, next_piece)

    ''' The snake eats an item. '''
    def eat(self, item):
        self.size += item.nutriment

    ''' Finds the head of the snake. '''
    def get_head(self):
        return self.pieces[self._findPreviousIndex(self.piecesIndex)]

    ''' Says if it touches the given snake. '''
    def touch(self, snake):
        touch = False

        # Head of the snake.
        head = self.get_head()

        for piece in snake.pieces:
            if head != piece:
                if piece.position == head.position:
                    touch = True
                    break

        return touch

    ''' Says if the snake is hover the position. '''
    def is_hover(self, position):
        contain = False

        for piece in self.pieces:
            if piece.position == position:
                contain = True
                break

        return contain

    ''' Finds the previous index. '''
    def _findPreviousIndex(self, index):
        previous_index = index - 1

        if previous_index < 0:
            previous_index = len(self.pieces) - 1

        return previous_index

    ''' Finds the next index. '''
    def _findNextIndex(self, index):
        next_index = index + 1

        if next_index > len(self.pieces) - 1:
            next_index = 0

        return next_index
