import random

import pygame

from src.item_manager import ItemManager
from src.position import Position
from src.runnable import Runnable
from src.snake import Snake
from src.texture import Texture
from src.utils import Tuple
from src.vector import Vector


class SnakeGame(Runnable):

    GRID = (15, 10)
    PADDING = 10
    HEADER = 68

    GRID_SIZE = Tuple.multiply(GRID, (Texture.GRID_SIZE,) * 2)
    WINDOW_SIZE = (GRID_SIZE[0] + 2 * PADDING, GRID_SIZE[1] + HEADER + PADDING)

    ''' Colors. '''

    BACKGROUND_COLOR = (82, 75, 55)

    TEXT_COLOR = (246, 225, 153)

    ''' Assets. '''

    BACKGROUND_IMAGE = "assets/images/background.jpg"

    PLAY_BUTTON_IMAGE = "assets/images/play.png"

    PAUSE_BUTTON_IMAGE = "assets/images/pause.png"

    APPLE_IMAGE = "assets/images/apple-logo.png"

    FONT = "assets/fonts/font.ttf"

    def __init__(self):
        super().__init__('Snake', SnakeGame.WINDOW_SIZE)

        self.paused = False
        self.lost = False

        self.score = 0

        self.texture = Texture((SnakeGame.PADDING, SnakeGame.HEADER, SnakeGame.PADDING, SnakeGame.PADDING))
        self.items = None
        self.snake = None

        self.init()

        # Click zones.
        self.pause_area = pygame.Rect(SnakeGame.WINDOW_SIZE[0] - SnakeGame.HEADER, 0, SnakeGame.HEADER, SnakeGame.HEADER)

    ''' Initialize the game. '''

    def init(self):
        self.score = 0
        self.items = ItemManager(3, self.texture)
        self.snake = Snake(Position(0, 0), (1, 0), 3, self.texture)

        # Generates the items.
        self.items.generateMissingItems(self._find_random_free_position)

    def event(self, event):
        if event.type == pygame.KEYDOWN:
            if not self.paused:
                if event.key == pygame.K_UP:
                    self.snake.direction = Vector.UP

                elif event.key == pygame.K_DOWN:
                    self.snake.direction = Vector.DOWN

                elif event.key == pygame.K_RIGHT:
                    self.snake.direction = Vector.RIGHT

                elif event.key == pygame.K_LEFT:
                    self.snake.direction = Vector.LEFT

            if event.key == pygame.K_SPACE:
                self._togglePause()

        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                if self.pause_area.collidepoint(event.pos):
                    self._togglePause()

    def update(self):

        if not self.paused and not self.lost:

            # Make the move.
            self.snake.move(SnakeGame.GRID)

            if self.snake.touch(self.snake):
                self.lost = True
            else:
                item = self.items.find_item_at(self.snake.get_head().position)

                if item is not None:
                    self.score += item.nutriment

                    self.snake.eat(item)
                    self.items.remove(item)
                    self.items.generateMissingItems(self._find_random_free_position)

    ''' Finds a position that is not already occupied. '''

    def _find_random_free_position(self):

        position = None

        while position is None or self.items.has_item_at(position) or self.snake.is_hover(position):
            x = random.randrange(0, SnakeGame.GRID[0])
            y = random.randrange(0, SnakeGame.GRID[1])

            position = [x, y]

        return position

    def render(self):
        # Clear the screen.
        self.screen.fill(SnakeGame.BACKGROUND_COLOR)

        # Render the background.
        self._render_image(
            SnakeGame.BACKGROUND_IMAGE,
            (SnakeGame.PADDING, SnakeGame.HEADER),
            SnakeGame.GRID_SIZE)

        # Pause button.
        if self.paused or self.lost:
            button_asset = SnakeGame.PLAY_BUTTON_IMAGE
        else:
            button_asset = SnakeGame.PAUSE_BUTTON_IMAGE

        self._render_image(
            button_asset,
            (SnakeGame.WINDOW_SIZE[0] - 50, SnakeGame.HEADER / 2 - 17),
            (35, 35))

        # Apple icon.
        self._render_image(SnakeGame.APPLE_IMAGE, (15, SnakeGame.HEADER / 2 - 15), (30, 30))

        # Score.
        def position_score(text):
            return 54, SnakeGame.HEADER / 2 - text.get_height() / 2

        self._render_text(str(self.score), 30, SnakeGame.TEXT_COLOR, position_score)

        # Game title.
        def position_title(text):
            return SnakeGame.WINDOW_SIZE[0] / 2 - text.get_width() / 2,\
                   SnakeGame.HEADER / 2 - text.get_height() / 2

        self._render_text('SNAKE', 38, SnakeGame.TEXT_COLOR, position_title)

        # Render the items.
        self.items.draw(self.screen)

        # Render the snake.
        self.snake.draw(self.screen)

    ''' Renders an image. '''

    def _render_image(self, image, position, size):
        background = pygame.image.load(image)

        self.screen.blit(background, pygame.Rect(position, size))

    ''' Renders some text. '''

    def _render_text(self, text, font_size, font_color, position):
        font = pygame.font.Font(SnakeGame.FONT, font_size)
        text = font.render(text, True, font_color)

        self.screen.blit(text, position(text))

    ''' Toggle the pause. '''

    def _togglePause(self):

        if self.lost:
            self.lost = False
            self.paused = False

            if self.items:
                del self.items

            if self.snake:
                del self.snake

            self.init()
        else:
            self.paused = not self.paused
